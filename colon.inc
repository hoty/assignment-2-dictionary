;colon.inc
%define PTR 0
 
%macro colon 2
%2: dq PTR 
db %1, 0
 
%define PTR %2
%endmacro