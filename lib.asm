;lib.asm
global exit
global string_length
global print_string
global print_err
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
 
section .text
 
exit:
	mov rax, 60
	syscall
 
string_length:
	xor rax, rax
.count:
	cmp byte [rdi+rax], 0
	je .end
	inc rax
	jmp .count
.end:
	ret
 
print_string:
	call string_length
	mov rsi, rdi
	mov rdx, rax
	mov rax, 1
	mov rdi, 1
	syscall
	ret
 
print_err:
	call string_length
	mov rsi, rdi
	mov rdx, rax
	mov rax, 1
	mov rdi, 2
	syscall
	ret
 
print_char:
	push rdi
	mov rdi, rsp
	call print_string
	pop rdi    
	ret
 
print_newline:
    mov rdi, 10
	call print_char
    ret
 
 
print_uint:
    mov rax, rdi
	mov rdi, rsp
	push 0
	sub rsp, 16
	dec rdi
	mov r8, 10
 
.loop:
	xor rdx, rdx
	div r8
	or rdx, 48
	dec rdi	
	mov [rdi], dl
	test rax, rax
	jnz .loop
 
	call print_string
	add rsp, 24
    ret
 
 
print_int:
	test rdi, rdi
	jns print_uint
	push rdi
	mov rdi, '-'
	call print_char
	pop rdi
	neg rdi
	call print_uint
    ret
 
string_equals:
	mov al, byte [rdi]
	cmp al, byte [rsi]
	jne .end_no
	inc rdi
	inc rsi
	test al, al
	jnz string_equals
	mov rax, 1
    ret
 
.end_no:
	xor rax, rax
	ret
 
read_char:
    push 0
	xor rax, rax
	xor rdi, rdi
	mov rsi, rsp
	mov rdx, 1
	syscall
	pop rax
    ret 
 
read_word:
	push r14
	push r15
	xor r14, r14
	mov r15, rsi
	dec r15
 
.first:
	push rdi
	call read_char
	pop rdi
	cmp al, ' '
	je .first
	cmp al, 9
	je .first
	cmp al, 10
	je .first
	cmp al, 13
	je .first
	test al, al
	jz .third
 
.second:
	mov byte [rdi + r14], al
	inc r14
	push rdi
	call read_char
	pop rdi
	cmp al, ' '
	je .third
	cmp al, 9
	je .third
	cmp al, 10
	je .third
	cmp al, 13
	je .third
	test al, al
	jz .third
	cmp r14, r15
	je .fourth
	jmp .second
 
.third:
	mov byte [rdi + r14], 0
	mov rax, rdi
	mov rdx, r14
	pop r15
	pop r14
    ret
 
.fourth:
	xor rax, rax
	pop r15
	pop r14
	ret
 
; rdi points to a string
; returns rax: number, rdx : length
parse_uint:
	mov r8, 10
	xor rax, rax
	xor rcx, rcx
.loop:
	movzx r9, byte [rdi + rcx]
	cmp r9b, '0'
	jb .end
	cmp r9b, '9'
	ja .end
	xor rdx, rdx
	mul r8
	and r9b, 0xf
	add rax, r9
	inc rcx
	jmp .loop
.end:
	mov rdx, rcx
    ret
 
; rdi points to a string
; returns rax: number, rdx : length
parse_int:
	mov al, byte [rdi]
	cmp al, '-'
	je .sign
	call parse_uint
	ret
 
.sign:
	inc rdi
	call parse_uint
	neg rax
	test rdx, rdx
	jz .err
	inc rdx
    ret
 
.err:
	xor rax, rax
	ret
 
 
string_copy:
	call string_length
	cmp rax, rdx
	jae .buf_ov
.loop:
	mov dl, byte[rdi]
	mov byte[rsi], dl
	inc rdi
	inc rsi
	test dl, dl
	jnz .loop
	ret
 
.buf_ov:
	xor rax, rax
    ret