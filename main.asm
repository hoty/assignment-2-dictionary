;main.asm
%include "words.inc"
%include "lib.inc"
 
%define BUFFER_SIZE 256

global _start
extern find_word
 
section .rodata
err_msg: db "No such key", 0
 
 
section .text
_start:
	push rbp
	mov rbp, rsp
	sub rsp, BUFFER_SIZE
	mov rdi, rsp
	call read_word
	mov rdi, rax
	mov rsi, PTR
	call find_word
	test rax, rax
	jz .err
	add rax, 8
	push rax
	mov rax, [rsp]
	mov rdi, rax
	call string_length
	pop rdi
	add rdi, rax
	inc rdi
	call print_string
	mov rsp, rbp
	pop rbp
	test rdi, rdi
	call exit
.err:
	mov rdi, err_msg
	call print_err
	mov rsp, rbp
	pop rbp
	test rdi, rdi
	call exit
