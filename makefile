;makefile
ASM=nasm
ASMFL=-f elf64
 
prog: main.o lib.o dict.o
	ld -o $@ $+
 
main.o: main.asm colon.inc lib.inc wwords.inc
	$(ASM) $(ASMFL) -o $@ $<
 
dict.o: dict.asm lib.o
	$(ASM) $(ASMFL) -o $@ $<
 
lib.o: lib.asm
	$(ASM) $(ASMFL) -o $@ $<
 
clean:
	rm -rf *.o prog